<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    public function verify($token)
    {
    	$user = User::whereHas('email_verification', function($query) use ($token) {
    		$query->where('token', $token)
    			  ->where('expired_at', '>', time());
    	})->first();

    	if ($user) {
    		$user->verified_at = time();
    		$user->save();
    		return redirect('login');
    	}
    }
}
